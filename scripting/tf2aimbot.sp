#include <sourcemod>
#include <sdktools>
#include <sdkhooks>

new ClientEyes[MAXPLAYERS+1];
new ActiveWeapon[MAXPLAYERS+1];
new sprmdl;

public OnPluginStart() {
	for(new i = 1; i <= MaxClients; i++)
	{
		if(IsClientInGame(i))
		{
			SDKHook(i, SDKHook_WeaponCanSwitchToPost, WeaponSwitch);
			if(IsPlayerAlive(i))
				CreateEyeProp(i);
		}
	}
	HookEvent("player_spawn", Event_Spawn);
	HookEvent("player_death", Event_Death);
}

public OnPluginEnd()
{
	for(new i = 1; i <= MaxClients; i++)
	{
		if(IsSprite(ClientEyes[i]))
		{
			AcceptEntityInput(ClientEyes[i], "kill");
		}
	}
}

public OnMapStart() {
	sprmdl = PrecacheModel("effects/strider_bulge_dudv_dx60.vmt");
}

public OnClientPutInServer(client)
{
	SDKHook(client, SDKHook_WeaponCanSwitchToPost, WeaponSwitch);
}

public OnClientDisconnect(client)
{
	if(IsSprite(ClientEyes[client]))
	{
		AcceptEntityInput(ClientEyes[client], "Kill");
	}
}

public Action:WeaponSwitch(client, weapon)
{
	if(IsValidEntity(weapon))
	{
		ActiveWeapon[client] = GetEntProp(weapon, Prop_Send, "m_iItemDefinitionIndex"); 
	}
	return Plugin_Continue;
}

public Action:Event_Spawn(Handle:event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	if(IsPlayerAlive(client))
	{
		CreateEyeProp(client);
	}
}

public Action:Event_Death(Handle:event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	if(IsSprite(ClientEyes[client]))
	{
		AcceptEntityInput(ClientEyes[client], "kill");
	}
}

stock CreateEyeProp(client)
{
	if(IsSprite(ClientEyes[client]))
	{
		AcceptEntityInput(ClientEyes[client], "kill");
	}
	new DProp = CreateEntityByName("env_sprite");
	if(DProp > 0 && IsValidEntity(DProp))
	{
		DispatchKeyValue(DProp, "classname", "env_sprite");
		DispatchKeyValue(DProp, "spawnflags", "1");
		DispatchKeyValue(DProp, "rendermode", "0");
		DispatchKeyValue(DProp, "rendercolor", "0 0 0");
		
		DispatchKeyValue(DProp, "model", "effects/strider_bulge_dudv_dx60.vmt");
		SetVariantString("!activator");
		AcceptEntityInput(DProp, "SetParent", client, DProp, 0);
		SetVariantString("head");
		AcceptEntityInput(DProp, "SetParentAttachment", DProp , DProp, 0);
		DispatchSpawn(DProp);
		ClientEyes[client] = EntIndexToEntRef(DProp);
		SDKHook( DProp, SDKHook_SetTransmit, OnShouldProp);
		TeleportEntity(DProp, Float:{0.0,0.0,-4.0}, NULL_VECTOR, NULL_VECTOR);

	}
}

public Action:OnShouldProp( Ent, Client)
{
	return Plugin_Handled;
} 

public Action:OnPlayerRunCmd(client, &buttons, &impulse, Float:velocity[3], Float:angles[3], &weapon)
{
	static NextTargetTime[MAXPLAYERS+1];
	static Target[MAXPLAYERS+1];
	if(!IsClientInGame(client))
		return Plugin_Continue;
	if (buttons & IN_ATTACK3)
	{
		decl Float:clienteyes[3];
		decl Float:targetEyes[3];
		GetClientEyePosition(client, clienteyes);
		if(NextTargetTime[client] < GetTime())
		{
			Target[client] = Client_GetClosest(client, clienteyes);
			NextTargetTime[client] = GetTime() + 5;
		}
		if(!IsValidClient(Target[client]) || !IsPlayerAlive(Target[client]))
		{
			Target[client] = Client_GetClosest(client, clienteyes);
			NextTargetTime[client] = GetTime() + 5;
		}
		else
		{
			GetClientEyePosition(Target[client], targetEyes);
			if(!IsPointVisible(clienteyes, targetEyes))
			{
				Target[client] = Client_GetClosest(client, clienteyes);
				NextTargetTime[client] = GetTime() + 5;
			}
		}
		if(Target[client] > 0)
		{
			if(IsSprite(ClientEyes[Target[client]]))
			{
				decl Float:camangle[3],Float:vec[3];
				GetEntPropVector(ClientEyes[Target[client]], Prop_Data, "m_vecAbsOrigin", targetEyes);
				
				TE_SetupGlowSprite(targetEyes, sprmdl, 0.1, 0.1, 200);
				TE_SendToClient(client);
				
				switch(ActiveWeapon[client])
				{
					case 56,1005,1092:
						targetEyes[2] += GetVectorDistance(clienteyes, targetEyes)/75.0; //Adds the curve
				}
				
				MakeVectorFromPoints(targetEyes, clienteyes, vec);
				GetVectorAngles(vec, camangle);
				camangle[0] *= -1.0;
				camangle[1] += 180.0;
				ClampAngle(camangle);
				TeleportEntity(client, NULL_VECTOR, camangle, NULL_VECTOR);
			}
			else Target[client] = 0;
		}
	}
	return Plugin_Continue;
}

stock Client_GetClosest(client, Float:vecOrigin_center[3])
{	
	decl Float:vecOrigin_edict[3];
	new Float:distance = -1.0;
	new closestEdict = -1;
	new Float:edict_distance;
	new clientteam = GetClientTeam(client);
	for (new i = 1; i <= MaxClients; ++i) {
		if (!IsClientInGame(i) || !IsPlayerAlive(i) || (i == client) || GetClientTeam(i) == clientteam)
			continue;
		if(!IsSprite(ClientEyes[i]))
			continue
			
		GetEntPropVector(i, Prop_Data, "m_vecOrigin", vecOrigin_edict);
		GetClientEyePosition(i, vecOrigin_edict);
			
		edict_distance = GetVectorDistance(vecOrigin_center, vecOrigin_edict);

		if(IsPointVisible(vecOrigin_center, vecOrigin_edict))
		{
			if((edict_distance < distance) || (distance == -1.0)) {
				distance = edict_distance;
				closestEdict = i;
			}
		}
	}
	return closestEdict;
}

stock bool:IsSprite(Ent)
{
	if(Ent != -1 && IsValidEdict(Ent) && IsValidEntity(Ent) && IsEntNetworkable(Ent))
	{
		decl String:ClassName[255];
		GetEdictClassname(Ent, ClassName, 255);
		if(StrEqual(ClassName, "env_sprite"))
			return true;
	}
	return false;
}

stock ClampAngle(Float:fAngles[3])
{
	while(fAngles[0] > 89.0)  fAngles[0]-=360.0;
	while(fAngles[0] < -89.0) fAngles[0]+=360.0;
	while(fAngles[1] > 180.0) fAngles[1]-=360.0;
	while(fAngles[1] <-180.0) fAngles[1]+=360.0;
}

bool:IsValidClient( client ) 
{
    if ( !( 1 <= client <= MaxClients ) || !IsClientInGame(client) ) 
		return false; 
    return true; 
}

stock bool:IsPointVisible(const Float:start[3], const Float:end[3])
{
    TR_TraceRayFilter(start, end, MASK_SOLID, RayType_EndPoint, TraceEntityFilterStuff);
    return TR_GetFraction() >= 1.0;
}

public bool:TraceEntityFilterStuff(entity, mask)
{
    return entity > MaxClients;
}